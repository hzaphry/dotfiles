# ~/.bashrc HZ-Version

[[ $- != *i* ]] && return

PS1='\[\033[01;32m\]\u@\h\[\033[01;37m\]:\w\[\033[01;32m\] \$\[\033[00m\] '
function _update_ps1() {
    PS1=$(powerline-shell $?)
}
if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
fi

# ALIASES
#if [[ -f ~/.bash_aliases ]]; then 
#	. ~/.bash_aliases 
#fi
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias ls='exa' # --group-directories-first'
alias la='ls -a'
alias ll='ls -ahl'
alias ..='cd ..'
alias mv='mv -iv'
alias rm='rm -iv'
alias cp="cp -iv"
alias df='df -h'
alias free='free -m'
alias nv='nvim'
alias gh='history | grep -i'
alias src='. .bashrc'
alias left='ls -t -1'
#alias count variable='var | wc -l'
#alias git commands
#alias np='nano -w PKGBUILD'
alias more='less'
alias cmatrix='cmatrix -sbC cyan'
alias wget="wget --hsts-file=$XDG_CACHE_HOME/wget-hsts"
alias mocp='mocp -M "$XDG_CONFIG_HOME"/moc -O MOCDir="$XDG_CONFIG_HOME"/moc'
alias hzyd='yay -Syu --noconfigrm'
alias hzud='sudo pacman -Syyu'
alias ztop='bpytop'
alias pubip='curl ifconfig.me'
alias timeshift='timeshift-launcher'
alias shred='shred -zvun 4'
alias 'wttr-s'='curl wttr.in/29.97,32.55'
alias 'wttr-t'='curl wttr.in/29.97,32.55?format="%C+%t+=>+%f+|+%m+%M+|+%h+|+%w\n"'
alias wttr='curl wttr.in'

# git bare dotfiles
alias dots='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
alias dotpush='dots push git@gitlab.com:hzaphry/dotfiles.git'
alias dotup='dots add -u'

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/hz/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/hz/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/home/hz/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/hz/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

#colorscript -r
date +%a\ %F\ %H:%M
wttr-t
