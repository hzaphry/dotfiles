#
# ~/.profile
#

PATH=~/.local/bin:$PATH
RANGER_DEVICONS_SEPARATOR="  "
export PAGER='most'
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_STATE_HOME=${XDG_STATE_HOME:="$HOME/.local/state"}
export MPLAYER_HOME="$XDG_CONFIG_HOME/mplayer"
export LESSHISTFILE="$XDG_CACHE_HOME/less/history"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0"
export HISTFILE="$XDG_STATE_HOME/bash/history"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export VSCODE_PORTABLE="$XDG_DATA_HOME"/vscode
export CONDARC="$XDG_CONFIG_HOME/conda/condarc"

USERXSESSION="$XDG_CACHE_HOME/X11/xsession"
USERXSESSIONRC="$XDG_CACHE_HOME/X11/xsessionrc"
ALTUSERXSESSION="$XDG_CACHE_HOME/X11/Xsession"
ERRFILE="$XDG_CACHE_HOME/X11/xsession-errors"
#moving the Xauthority file brakes lightdm. So, don't.
#export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority"

