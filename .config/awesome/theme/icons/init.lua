local dir = os.getenv('HOME') .. '/.config/awesome/theme/icons'

return {
  --tags
  chrome = dir .. '/brave.svg',
  code = dir .. '/code-braces.svg',
  menu = dir .. '/menu.svg',
  social = dir .. '/forum.svg',
  folder = dir .. '/folder.png',
  music = dir .. '/music.svg',
  game = dir .. '/google-controller.svg',
  lab = dir .. '/flask.svg',
  brush = dir .. '/brush.png',
  studio = dir .. '/studio.png',
  tweeks = dir .. '/tweeks.png',
  house = dir .. '/home.png',
  --others
  stamp = dir .. '/Archlinux-icon.svg',
  close = dir .. '/close.svg',
  logout = dir .. '/logout.svg',
  sleep = dir .. '/power-sleep.svg',
  power = dir .. '/power.svg',
  hiber = dir .. '/hiber.png',
  lock = dir .. '/lock.svg',
  restart = dir .. '/restart.svg',
  search = dir .. '/magnify.svg',
  volume = dir .. '/volume-high.svg',
  brightness = dir .. '/brightness-7.svg',
  chart = dir .. '/chart-areaspline.svg',
  memory = dir .. '/memory.svg',
  harddisk = dir .. '/harddisk.svg',
  thermometer = dir .. '/thermometer.svg',
  plus = dir .. '/plus.svg',
  left_arrow = dir .. '/left-arrow.png',
  right_arrow = dir .. '/right-arrow.png',
}
