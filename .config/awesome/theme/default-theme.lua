local filesystem = require('gears.filesystem')
local mat_colors = require('theme.mat-colors')
local theme_dir = filesystem.get_configuration_dir() .. '/theme'
local gears = require('gears')
local dpi = require('beautiful').xresources.apply_dpi
local theme = {}
theme.icons = theme_dir .. '/icons/'
theme.font = 'Roboto medium 10'

-- Colors Pallets

-- Primary
theme.primary = mat_colors.deep_orange

-- Accent
theme.accent = mat_colors.pink

-- Background
theme.background = mat_colors.grey

local awesome_overrides =
  function(theme)
  theme.dir = os.getenv('HOME') .. '/.config/awesome/theme'
  theme.icons = theme.dir .. '/icons/'
  --theme.wallpaper = theme.dir .. '/wallpapers/DarkCyan.png'
  theme.wallpaper = '#e0e0e0'
  --theme.font = 'Roboto medium 10'
  theme.title_font = 'Roboto medium 14'

  theme.fg_normal = '#ffffffde'
  theme.fg_focus = '#e4e4e4'
  theme.fg_urgent = '#CC9393'
  theme.bat_fg_critical = '#232323'

  theme.bg_normal = theme.background.hue_800
  theme.bg_focus = '#5a5a5a'
  theme.bg_urgent = '#3F3F3F'
  theme.bg_systray = nil --theme.background.hue_800

  -- Borders

  theme.border_width = dpi(2)
  theme.border_normal = theme.background.hue_800
  theme.border_focus = theme.primary.hue_300
  theme.border_marked = '#CC9393'

  -- Menu

  theme.menu_height = dpi(18)
  --shape = gears.shape.rounded_rect(cr, w, h, 8)
  theme.menu_width = dpi(140)
  --theme.menu_font = 'Cantarell 11'
  --theme.menu_font = 'Roboto Mono 11'
  theme.menu_fg_normal = '#bbc2cf'
  theme.menu_bg_normal = '#272935'
  theme.menu_fg_focus = '#f1fa8c'
  theme.menu_bg_focus = '#44475a'
  --[[
  theme.menu_bg_normal = theme.primary.hue_800
  theme.menu_fg_normal = theme.fg_normal
  theme.menu_fg_focus = '#98be65'
  theme.menu_fg_normal = '#FF79C6'
  color0 = '#c678dd', --Purple
	color1 = '#bbc2cf', --Grey
	color2 = '#51afef', --Blue
	color3 = '#98be65', --Green
  color4 = '#ebe6ef', --White
]]

  -- Tooltips
  theme.tooltip_bg = '#232323'
  --theme.tooltip_border_color = '#232323'
  theme.tooltip_border_width = 0
  theme.tooltip_shape = function(cr, w, h)
    gears.shape.rounded_rect(cr, w, h, dpi(6))
  end

  -- Layout

  theme.layout_max = theme.icons .. 'layouts/layout-max.png'
  theme.layout_floating = theme.icons .. 'layouts/layout-floating.png'
  theme.layout_tile = theme.icons .. 'layouts/layout-monadtall.png'

  -- Taglist

  theme.taglist_bg_empty = theme.background.hue_800
  theme.taglist_bg_occupied =
    'linear:0,0:' ..
    dpi(40) ..
      ',0:0,' ..
        theme.occupied.hue_200 ..
          ':0.08,' .. theme.occupied.hue_600 .. ':0.08,' .. theme.background.hue_800 .. ':1,' .. theme.background.hue_800
--  theme.taglist_fg_urgent = theme.accent.hue_500
  theme.taglist_bg_urgent = --theme.accent.hue_500
    'linear:0,0:' ..
    dpi(40) ..
      ',0:0,' ..
        theme.accent.hue_500 ..
          ':0.08,' .. theme.accent.hue_500 .. ':0.08,' .. theme.background.hue_800 .. ':1,' .. theme.background.hue_800
  theme.taglist_bg_focus = --theme.primary.hue_700
'linear:0,0:' ..
    dpi(40) ..
      ',0:0,' ..
        theme.primary.hue_500 ..
          ':0.08,' .. theme.primary.hue_500 .. ':0.08,' .. theme.background.hue_800 .. ':1,' .. theme.background.hue_800

  -- Tasklist

  --theme.tasklist_font = 'Roboto Medium 11'
  theme.tasklist_bg_normal = theme.background.hue_800
  theme.tasklist_bg_focus =
    'linear:0,0:0,' ..
    dpi(40) ..
      ':0,' ..
        theme.background.hue_800 ..
          ':0.95,' .. theme.background.hue_800 .. ':0.95,' .. theme.fg_normal .. ':1,' .. theme.fg_normal
  theme.tasklist_bg_urgent = theme.primary.hue_800
  theme.tasklist_fg_focus = '#DDDDDD'
  theme.tasklist_fg_urgent = theme.fg_normal
  theme.tasklist_fg_normal = '#AAAAAA'

  --Client
  theme.border_width = dpi(2)
  theme.border_focus = theme.primary.hue_500
  theme.border_normal = theme.background.hue_800
end
return {
  theme = theme,
  awesome_overrides = awesome_overrides
}
