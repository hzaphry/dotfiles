local gears = require('gears')
local awful = require('awful')
local wibox = require("wibox")
require('awful.autofocus')
local hotkeys_popup = require('awful.hotkeys_popup').widget
local beautiful = require('beautiful')
local menubar = require('menubar')
local freedesktop = require('freedesktop')
local apps = require('configuration.apps')

local menu1 = {
    {"Calculator", apps.default.calculator},
    {"Browser-Brave", apps.default.browser},
    {"Files", apps.default.files},
    {"slack", apps.default.slack},
    {"VLC", apps.default.music},
    {"Office", apps.default.office}
--    {"Office", apps.default.office},
}
return {
    -- Menu
    mainmenu = freedesktop.menu.build({
        before = {
            {"Hot Keys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end},
            --{"Applications", menu1},
            {"Refresh", _G.awesome.restart},
            {"Terminal", apps.default.terminal},
            {"dmenu", apps.default.dmenu},
            {"Change Wallpaper", apps.default.variety},
            --{"Screen Saver", apps.default.game},
        },
        after = {
            {"display", "arandr"},
            {"Exit", function() _G.exit_screen_show() end}
        }
    })
}

--[[
    return {
    -- Menu
    mainmenu = awful.menu({
        items = {
            {"  Hot Keys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end},
            {"  Applications", menu1},
            {"  Terminal", apps.default.terminal},
            {"  Launcer-dmenu", apps.default.dmenu},
            {"  Change Wallpaper", apps.default.variety},
            {"﨤  Screen Saver", apps.default.game},
            {"  Exit", function() _G.exit_screen_show() end}
        }
    })
}
]]