local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local TaskList = require('widget.task-list')
local TagList = require('widget.tag-list')
local gears = require('gears')
local clickable_container = require('widget.material.clickable-container')
local mat_icon_button = require('widget.material.icon-button')
local mat_icon = require('widget.material.icon')
local dpi = require('beautiful').xresources.apply_dpi
local icons = require('theme.icons')
local mainmenu = require('module.mainmenu')
local apps = require('configuration.apps')
local tray_toggler = require('widget.tray-toggle')
--[[
local battery = require('widget.battery')
local bluetooth = require('widget.bluetooth')
local brightness = require('widget.brightness.brightness-slider')
local cpu = require('widget.cpu.cpu-meter')
local ram = require('widget.ram.ram-meter')
local temperature = require('widget.temperature')
local volume = require('widget.volume')
local harddrive = require('widget.harddrive')
local package-updater = require('widget.package-updater')
local wifi = require('widget.wifi')
]]


-- Clock / Calendar 24h format
-- local textclock = wibox.widget.textclock('<span font="Roboto Mono bold 9">%d.%m.%Y\n     %H:%M</span>')
-- Clock / Calendar 12AM/PM fornat
local textclock = wibox.widget.textclock('<span font="Roboto Mono 11">%b-%d %a %H:%M</span>')
textclock.forced_height = 24

-- Add a calendar (credits to kylekewley for the original code)
local month_calendar = awful.widget.calendar_popup.month({
  screen = s,
  start_sunday = true,
  week_numbers = false
})
month_calendar:attach(textclock)

local clock_widget = wibox.container.margin(textclock, dpi(2), dpi(2), dpi(2), dpi(2))

local keyboardlayout = awful.widget.keyboardlayout()
keyboardlayout.forced_height = dpi(24)
keyboardlayout.forced_width = 24

awful.screen.connect_for_each_screen(
  function(s)
    s.systray = wibox.widget.systray()
    s.systray.visible = true
    s.systray:set_horizontal(true)
    s.systray:set_base_size(26)
    s.systray.forced_height = 26
  end
)

-- [[Titus - Horizontal Tray
local Systray = function(s)
  local systray = s.systray
  return systray
end

-- Create an imagebox widget which will contains an icon indicating which layout we're using.
-- We need one layoutbox per screen.
local LayoutBox = function(s)
  local layoutBox = clickable_container(awful.widget.layoutbox(s))
  layoutBox:buttons(
    awful.util.table.join(
      awful.button(
        {},
        1,
        function()
          awful.layout.inc(1)
        end
      ),
      awful.button(
        {},
        3,
        function()
          awful.layout.inc(-1)
        end
      ),
      awful.button(
        {},
        4,
        function()
          awful.layout.inc(-1)
        end
      ),
      awful.button(
        {},
        5,
        function()
          awful.layout.inc(1)
        end
      )
    )
  )
  return layoutBox
end

local sep = wibox.widget {
  widget = wibox.widget.separator,
  color = "#777777",
  forced_height = 20,
  forced_width = 2
}

local TopPanel = function(s)
  
    local panel =
    wibox(
    {
      ontop = true,
      screen = s,
      height = dpi(26),
      width = s.geometry.width-35,
      x = s.geometry.x+35,
      y = s.geometry.y,
      stretch = false,
      bg = beautiful.background.hue_800,
      fg = beautiful.fg_normal,
      struts = {
        top = dpi(26),
      }
    }
    )

    panel:struts(
      {
        top = dpi(26),
      }
    )

    panel:setup {
      layout = wibox.layout.align.horizontal,
      {
        layout = wibox.layout.fixed.horizontal,
        -- Create a taglist widget
        TaskList(s),
      },
      nil,
      {
        layout = wibox.layout.fixed.horizontal,
        Systray(s),--wibox.container.margin(Systray(s), dpi(2), dpi(2), dpi(2), dpi(2)),
        keyboardlayout,
        sep,
        clock_widget,-- Clock
        LayoutBox(s),-- Layout box
        spacing = 5
      },
--[[        tray_toggler,
        s.screen_rec,
        --cpu
        s.mpd,
        s.network,
        s.bluetooth,
        s.battery,
        --battery,
        --brightness,
        layout_box,
        s.info_center_toggle]]
    }
  return panel
end

return TopPanel
