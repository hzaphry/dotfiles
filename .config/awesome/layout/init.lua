local awful = require('awful')
local top_panel = require('layout.top-panel')
local side_panel = require('layout.side-panel')
local top_only_panel = require('layout.top-only-panel')
-- Create a wibox for each screen and add it
awful.screen.connect_for_each_screen(
  function(s)
  -- Launch one bar at top (make sure to commentout widget>tag-list line 160)
    s.top_only_panel = top_only_panel(s)
  -- Launch two bars (make sure to uncomment widget>tag-list line 160)
  --  s.top_panel = top_panel(s)
  --  s.side_panel = side_panel(s)
  end
)
-- Hide bars when app go fullscreen
function updateBarsVisibility()
  for s in screen do
    if s.selected_tag then
      local fullscreen = s.selected_tag.fullscreenMode
      -- Order matter here for shadow
      if s.top_only_panel then
        s.top_only_panel.visible = not fullscreen
      elseif s.top_panel then
        s.top_panel.visible = not fullscreen
        if s.side_panel then
          s.side_panel.visible = not fullscreen
        end
      end
    end
  end
end
_G.tag.connect_signal(
  'property::selected',
  function(t)
    updateBarsVisibility()
  end
)
_G.client.connect_signal(
  'property::fullscreen',
  function(c)
    c.screen.selected_tag.fullscreenMode = c.fullscreen
    updateBarsVisibility()
  end
)
_G.client.connect_signal(
  'unmanage',
  function(c)
    if c.fullscreen then
      c.screen.selected_tag.fullscreenMode = false
      updateBarsVisibility()
    end
  end
)