local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local TaskList = require('widget.task-list')
local TagList = require('widget.tag-list')
local gears = require('gears')
local clickable_container = require('widget.material.clickable-container')
local mat_icon_button = require('widget.material.icon-button')
local mat_icon = require('widget.material.icon')
local dpi = require('beautiful').xresources.apply_dpi
local icons = require('theme.icons')
local apps = require('configuration.apps')

-- Generate Awesome icon:
local mylauncher = awful.widget.launcher({
  image = icons.stamp,
  --menu = apps.default.dmenu
  command = apps.default.dmenu
})

local exit_button = mat_icon_button(mat_icon(icons.power, dpi(35)))
exit_button:buttons(gears.table.join(awful.button(
      {}, 1, nil,
      function()
        _G.exit_screen_show()
      end
    )))
 
local add_button = mat_icon_button(mat_icon(icons.plus, dpi(40)))
add_button:buttons(
  gears.table.join(
    awful.button(
      {},
      1,
      nil,
      function()
        awful.spawn(
          awful.screen.focused().selected_tag.defaultApp,
          {
            tag = _G.mouse.screen.selected_tag,
            placement = awful.placement.bottom_right
          }
        )
      end
    )
  )
)
  
local sep = wibox.widget {
  widget = wibox.widget.separator,
  color = "#ffffff",
  forced_height = 10,
  forced_width = 2
}

local SidePanel = function(s)
    local panel = wibox({
      ontop = true,
      screen = s,
      height = s.geometry.height,
      width = dpi(35),
      x = s.geometry.x,
      y = s.geometry.y,
      stretch = false,
      bg = beautiful.background.hue_800,
      fg = beautiful.fg_normal,
      struts = {
        left = dpi(26)
      }
    })
    panel:struts(
      {
        left = dpi(35)
      }
    )
    panel:setup {
      layout = wibox.layout.align.vertical,
      {
        layout = wibox.layout.fixed.vertical,
        add_button,
        mylauncher,
        TagList(s),
      },
      nil,
      exit_button
    }
  return panel
end
return SidePanel
