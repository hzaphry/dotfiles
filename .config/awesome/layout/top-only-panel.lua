local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local TaskList = require('widget.task-list')
local TagList = require('widget.tag-list')
local gears = require('gears')
local clickable_container = require('widget.material.clickable-container')
local mat_icon_button = require('widget.material.icon-button')
local mat_icon = require('widget.material.icon')
local dpi = require('beautiful').xresources.apply_dpi
local icons = require('theme.icons')
local mainmenu = require('module.mainmenu')
local apps = require('configuration.apps')
local tray_toggler = require('widget.tray-toggle')
local battery_widget = require('widget.battery')
local brightness_widget = require('widget.brightness')
local volume_widget = require('widget.volume')
local tray_toggler = require('widget.tray-toggle')
local package_updater = require('widget.package-updater')
local wifi_widget = require('widget.wifi')
local cpu = require('widget.cpu')
--local task_list = require('widget.task-list')
--[[
local mpdarc_widget = require('widget.mpdarc')
local bluetooth = require('widget.bluetooth')
local ram = require('widget.ram.ram-meter')
local temperature = require('widget.temperature')
local harddrive = require('widget.harddrive')
]]


-- Clock / Calendar 24h format
-- local textclock = wibox.widget.textclock('<span font="Roboto Mono bold 9">%d.%m.%Y\n     %H:%M</span>')
-- Clock / Calendar 12AM/PM fornat
local textclock = wibox.widget.textclock('<span font="Roboto Mono 10">%H:%M</span>')
textclock.forced_height = 24

local exit_button = mat_icon_button(mat_icon(icons.power, dpi(22)))
exit_button:buttons(gears.table.join(awful.button(
      {}, 1, nil,
      function()
        _G.exit_screen_show()
      end
    )))

-- Add a calendar (credits to kylekewley for the original code)
local month_calendar = awful.widget.calendar_popup.month({
  screen = s,
  start_sunday = true,
  week_numbers = false
})
month_calendar:attach(textclock)

local clock_widget = wibox.container.margin(textclock, dpi(2), dpi(2), dpi(2), dpi(2))

local add_button = mat_icon_button(mat_icon(icons.plus, dpi(22)))
add_button:buttons(
  gears.table.join(
    awful.button(
      {},
      1,
      nil,
      function()
        awful.spawn(
          awful.screen.focused().selected_tag.defaultApp,
          {
            tag = _G.mouse.screen.selected_tag,
            placement = awful.placement.bottom_right
          }
        )
      end
    )
  )
)
  
local keyboardlayout = awful.widget.keyboardlayout()
-- keyboardlayout.forced_height = dpi(24)
-- keyboardlayout.forced_width = 24

awful.screen.connect_for_each_screen(
  function(s)
    s.systray = wibox.widget.systray()
    s.systray.visible = false
    s.systray.opacity = 0.1
    s.systray:set_horizontal(true)
    s.systray:set_base_size(20)
    s.systray.forced_height = 22
  end
)

-- [[Titus - Horizontal Tray
local Systray = function(s)
  local systray = s.systray
  return systray
end

-- Create an imagebox widget which will contains an icon indicating which layout we're using.
-- We need one layoutbox per screen.
local LayoutBox = function(s)
  local layoutBox = clickable_container(awful.widget.layoutbox(s))
  layoutBox:buttons(
    awful.util.table.join(
      awful.button(
        {},
        1,
        function()
          awful.layout.inc(1)
        end
      ),
      awful.button(
        {},
        3,
        function()
          awful.layout.inc(-1)
        end
      ),
      awful.button(
        {},
        4,
        function()
          awful.layout.inc(-1)
        end
      ),
      awful.button(
        {},
        5,
        function()
          awful.layout.inc(1)
        end
      )
    )
  )
  return layoutBox
end

local sep = wibox.widget {
  widget = wibox.widget.separator,
  color = "#777777",
  forced_height = 20,
  forced_width = 2
}

local TopPanel = function(s)
  
    local panel =
    wibox(
    {
      ontop = true,
      screen = s,
      height = dpi(22),
      width = s.geometry.width,
      x = s.geometry.x,
      y = s.geometry.y,
      stretch = false,
      bg = '#282a3600', --trans panal
      --bg = beautiful.background.hue_800,
      fg = beautiful.fg_normal,
      --opacity = 0.9,
      --alpha = 0.5,
      struts = {
        top = dpi(22),
      }
    }
    )

    panel:struts(
      {
        top = dpi(22),
      }
    )

    panel:setup {
      layout = wibox.layout.align.horizontal,
      {
        layout = wibox.layout.fixed.horizontal,
        -- Create a taglist widget
        TagList(s),
        add_button,
        TaskList(s),
      },
      nil,
      {
        layout = wibox.layout.fixed.horizontal,
        --mpdarc_widget,
        Systray(s),--wibox.container.margin(Systray(s), dpi(2), dpi(2), dpi(2), dpi(2)),
        tray_toggler,
        --task_list,
        --wifi_widget(),
        cpu(),
        volume_widget{widget_type='arc'},
        package_updater,
        brightness_widget(),
        battery_widget,
        keyboardlayout,
        clock_widget,-- Clock
        exit_button,
        LayoutBox(s),-- Layout box
        spacing = 4
      }
--[[    
        sep,
        s.screen_rec,
        s.network,
        s.bluetooth,
        s.info_center_toggle]]
    }
  return panel
end

return TopPanel
	