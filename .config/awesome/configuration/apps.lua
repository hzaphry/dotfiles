local filesystem = require('gears.filesystem')

-- Thanks to jo148 on github for making rofi dpi aware!
local with_dpi = require('beautiful').xresources.apply_dpi
local get_dpi = require('beautiful').xresources.get_dpi
-- rofi = rofi_command
-- local rofi_command = 'env /usr/bin/rofi -dpi ' .. get_dpi() .. ' -width ' .. with_dpi(400) .. ' -show drun -theme ' .. filesystem.get_configuration_dir() .. '/configuration/rofi.rasi -run-command "/bin/bash -c -i \'shopt -s expand_aliases; {cmd}\'"'
--local dmenu_command = 'dmenu_run -p "dmenu: " -nb "#0D1C38" -sb "#3498DB" -fn "Roboto Mono 12"'
--local dmenu_command = 'dmenu_run -p "dmenu: " -nf "#bbbbbb" -nb "#222222" -sf "#eeeeee" -sb "#005577" -fn "Roboto Mono 12"'
local dmenu_command = 'dmenu_run -p "dmenu: " -h 20 -nf "#FF79C6" -nb "#272935" -sf "#f1fa8c" -sb "#44475a" -fn "Roboto Mono 8"'
local dmenu_centered = 'dmenu_run -c -l 15 -p "dmenu: " -h 20 -nf "#FF79C6" -nb "#272935" -sf "#f1fa8c" -sb "#44475a" -fn "Roboto Mono 11"'
dmenu = dmenu_command
dmenu_center = dmenu_centered

return {
  -- List of apps to start by default on some actions
  default = {
    terminal = 'alacritty',
    rofi = rofi_command,
    dmenu = dmenu_command,
    dmenu_center = dmenu_centered,
    lock = 'i3lock-fancy',
    quake = 'alacritty',
    screenshot = 'flameshot screen -p ~/Pictures',
    region_screenshot = 'flameshot gui -p ~/Pictures',
    delayed_screenshot = 'flameshot screen -p ~/Pictures -d 5000',
    browser = 'brave',
    editor = 'gedit', -- gui text editor
    text = 'gedit',
    social = 'slack', -- 'hexchat', 'discord',
    slack = 'slack',
    game = 'alacritty --hold -e cmatrix -sb', -- rofi_command
    files = 'Thunar',
    ranger = 'alacritty -e ranger',
    --network_manager = 'nm-applet',
    network_manager = 'nm-connection-editor',
    music = 'vlc',
    variety = 'variety -n',
    virt = 'virt-manager',
    virtualBox = 'virtualbox -style Fusion %U',
    calculator = 'qalculate-gtk',
    office = 'libreoffice',
    moc = "alacritty -e mocp -M /home/hz/.config/moc -O MOCDir=/home/hz/.config/moc",
    mocp = 'mocp -G -M /home/hz/.config/moc -O MOCDir=/home/hz/.config/moc',
    mocf = 'mocp -f -M /home/hz/.config/moc -O MOCDir=/home/hz/.config/moc',
    mocb = 'mocp -r -M /home/hz/.config/moc -O MOCDir=/home/hz/.config/moc',
    mocicon = 'mocicon',
    obs = 'obs',
    studio = 'obs',
    kdenlive = 'kdenlive',
    brush = 'gimp',
    tweeks = 'gparted',
    house = dmenu_center
    --[[virt-manager", "Virt-manager
    # next wallpaper
    variety -n
    # previous wallpaper
    variety -p
    # favorite wallpaper
    variety -f
    # pause wallpaper
    variety --pause
    # resume wallpaper
    variety --resume
    ]]
  },
  -- List of apps to start once on start-up
  run_on_start_up = {
    'picom --experimental-backends',
    'picom --config ' .. filesystem.get_configuration_dir() .. '/configuration/picom.conf',
    'nm-applet --indicator', -- wifi
    --'volumeicon', --'pnmixer', -- shows an audiocontrol applet in y when installed.
    'xfce4-clipman', 'xfce4-notes',
    '/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 & eval $(gnome-keyring-daemon -s --components=pkcs11,secrets,ssh,gpg)', -- credential manager
    'xfce4-power-manager', -- Power manager
    'conky -c ~/.config/conky/conky1.conf',
    --'conky -c ~/.config/conky/conky2.conf',
    'flameshot',
    --'slack',
    --'feh --randomize --bg-fill ~/Photos/wallpapers/*',
    '/usr/bin/variety',
    --'mocp',
    --'mocicon',
    --'transmission-gtk',
    --'pamac-tray',
    --'blueberry-tray', -- Bluetooth tray icon
    --'numlockx on', -- enable numlock
    --'synology-drive -minimized',
    -- Add applications that need to be killed between reloads
    -- to avoid multipled instances, inside the awspawn script
    '~/.config/awesome/configuration/awspawn', -- Spawn "dirty" apps that can linger between sessions
  }
}
