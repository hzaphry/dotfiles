local awful = require('awful')
require('awful.autofocus')
local beautiful = require('beautiful')
local hotkeys_popup = require('awful.hotkeys_popup').widget
local modkey = require('configuration.keys.mod').modKey
local altkey = require('configuration.keys.mod').altKey
local apps = require('configuration.apps')
local mainmenu = require('module.mainmenu')

function useless_gaps_resize(thatmuch, s, t)
  local scr = s or awful.screen.focused()
  local tag = t or scr.selected_tag
  tag.gap = tag.gap + tonumber(thatmuch)
  awful.layout.arrange(scr)
end

-- Key bindings
local globalKeys = awful.util.table.join(
  -- Hotkeys
  awful.key({modkey}, 'F1', hotkeys_popup.show_help, {description = 'Show help', group = 'awesome'}),
  -- Tag browsing
  awful.key({modkey, 'Shift'}, 'Left', awful.tag.viewprev, {description = 'view previous', group = 'tag'}),
  awful.key({modkey, 'Shift'}, 'Right', awful.tag.viewnext, {description = 'view next', group = 'tag'}),
  awful.key({modkey, 'Shift'}, 'Down', function () useless_gaps_resize(1) end,
    {description = "increment useless gaps", group = "tag"}),
  awful.key({modkey, 'Shift'}, 'Up', function () useless_gaps_resize(-1) end,
    {description = "decrement useless gaps", group = "tag"}),
  -- awful.key({altkey, 'Control'}, 'Up', awful.tag.viewprev, {description = 'view previous', group = 'tag'}),
  -- awful.key({altkey, 'Control'}, 'Down', awful.tag.viewnext, {description = 'view next', group = 'tag'}),
  awful.key({modkey}, 'Escape', awful.tag.history.restore, {description = 'go back', group = 'tag'}),
  awful.key(
    {modkey},
    'd',
    function()
      awful.client.focus.byidx(1)
    end,
    {description = 'Focus next by index', group = 'client'}
  ),
  --[[
  awful.key(
    {modkey},
    'a',
    function()
      awful.client.focus.byidx(-1)
    end,
    {description = 'Focus previous by index', group = 'client'}
  ),]]
  awful.key(
    {modkey, 'Shift'},
    'Return',
    function()
      awful.spawn(apps.default.dmenu)
    end,
    {description = 'Quick dmenu-bar', group = 'awesome'}
  ),
  awful.key(
     {modkey, 'Shift'},
     'space',
     function()
       awful.spawn(apps.default.dmenu_center)
     end,
     {description = 'Center dmenu-window', group = 'awesome'}
  ),
  awful.key(
    {modkey},
    'm',
    function()
      mainmenu.mainmenu:show()
    end,
    {description = 'Show main menu', group = 'awesome'}
  ),
  --[[awful.key(
    {modkey, 'Shift'},
    'r',
    function()
      awful.spawn('reboot')
    end,
    {description = 'Reboot Computer', group = 'awesome'}
  ),
  Rawful.key(
    {modkey, 'Shift'},
    's',
    function()
      awful.spawn('shutdown now')
   end,
    {description = 'Shutdown Computer', group = 'awesome'}
  ),]]
  awful.key(
    {modkey, 'Shift'},
    'l',
    function()
      _G.exit_screen_show()
    end,
    {description = 'Log Out Screen', group = 'awesome'}
  ),
  awful.key({modkey}, 'u', awful.client.urgent.jumpto, {description = 'jump to urgent client', group = 'client'}),
  -- Default screen focus
  awful.key(
    {modkey},
    'Tab',
    function()
      awful.screen.focus_relative(1)
    end,
    {description = 'Switch to next screen', group = 'client'}
  ),
  -- Default client focus
  awful.key(
    {altkey},
    'Tab',
    function()
      --awful.client.focus.history.previous()
      awful.client.focus.byidx(1)
      if _G.client.focus then
        _G.client.focus:raise()
      end
    end,
    {description = 'Switch to next window', group = 'client'}
  ),
  awful.key(
    {altkey, 'Shift'},
    'Tab',
    function()
      --awful.client.focus.history.previous()
      awful.client.focus.byidx(-1)
      if _G.client.focus then
        _G.client.focus:raise()
      end
    end,
    {description = 'Previous window', group = 'client'}
  ),
  -- Programms
  awful.key(
    {modkey},
    'l',
    function()
      awful.spawn(apps.default.lock)
    end,
    {description = 'Lock the screen', group = 'awesome'}
  ),
  awful.key(
    {modkey},
    'Print',
    function()
      awful.util.spawn_with_shell(apps.default.delayed_screenshot)
    end,
    {description = 'Area screenshot +10s', group = 'screenshots (clipboard)'}
  ),
  awful.key(
    {modkey},
    'p',
    function()
      awful.util.spawn_with_shell(apps.default.screenshot)
    end,
    {description = 'Screenshot active monitor', group = 'screenshots (clipboard)'}
  ),
  awful.key(
    {altkey, 'Shift'},
    'p',
    function()
      awful.util.spawn_with_shell(apps.default.region_screenshot)
    end,
    {description = 'Area screenshot', group = 'screenshots (clipboard)'}
  ),
  awful.key(
    {modkey},
    'r',
    function()
      awful.util.spawn(apps.default.editor)
    end,
    {description = 'Open a text/code editor', group = 'launcher'}
  ),
  awful.key(
    {modkey},
    'k',
    function()
      awful.util.spawn(apps.default.kdenlive)
    end,
    {description = 'Kdenlive', group = 'launcher'}
  ),
  awful.key(
    {modkey},
    'o',
    function()
      awful.util.spawn(apps.default.obs)
    end,
    {description = 'OpenBroadcastSoft "OBS"', group = 'launcher'}
  ),
  awful.key(
    {modkey},
    'b',
    function()
      awful.util.spawn(apps.default.browser)
    end,
    {description = 'Open a browser', group = 'launcher'}
  ),
  -- Standard program
  awful.key(
    {modkey},
    'Return',
    function()
      awful.spawn(apps.default.terminal)
    end,
    {description = 'Open a terminal', group = 'launcher'}
  ),
  awful.key({modkey, 'Shift'}, 'r', _G.awesome.restart, {description = 'reload awesome', group = 'awesome'}),
  -- awful.key({modkey, 'Control'}, 'q', _G.awesome.quit, {description = 'quit awesome', group = 'awesome'}),
  awful.key(
    {altkey, 'Shift'},
    'Right',
    function()
      awful.tag.incmwfact(0.05)
    end,
    {description = 'Increase master width factor', group = 'layout'}
  ),
  awful.key(
    {altkey, 'Shift'},
    'Left',
    function()
      awful.tag.incmwfact(-0.05)
    end,
    {description = 'Decrease master width factor', group = 'layout'}
  ),
  awful.key(
    {altkey, 'Shift'},
    'Down',
    function()
      awful.client.incwfact(0.05)
    end,
    {description = 'Decrease master height factor', group = 'layout'}
  ),
  awful.key(
    {altkey, 'Shift'},
    'Up',
    function()
      awful.client.incwfact(-0.05)
    end,
    {description = 'Increase master height factor', group = 'layout'}
  ),
  awful.key(
    {modkey, 'Control'},
    'Left',
    function()
      awful.tag.incnmaster(1, nil, true)
    end,
    {description = 'Increase the number of master clients', group = 'layout'}
  ),
  awful.key(
    {modkey, 'Control'},
    'Right',
    function()
      awful.tag.incnmaster(-1, nil, true)
    end,
    {description = 'Decrease the number of master clients', group = 'layout'}
  ),
  awful.key(
    {modkey, 'Control'},
    '=',
    function()
      awful.tag.incncol(1, nil, true)
    end,
    {description = 'Increase the number of columns', group = 'layout'}
  ),
  awful.key(
    {modkey, 'Control'},
    '-',
    function()
      awful.tag.incncol(-1, nil, true)
    end,
    {description = 'Decrease the number of columns', group = 'layout'}
  ),
  awful.key(
    {modkey},
    'space', -- 'Tab'
    function()
      awful.layout.inc(1)
    end,
    {description = 'Select next', group = 'layout'}
  ),
  -- awful.key(
  --   {modkey, 'Shift'},
  --   'space',
  --   function()
  --     awful.layout.inc(-1)
  --   end,
  --   {description = 'Select previous', group = 'layout'}
  -- ),
  awful.key(
    {modkey, 'Control'},
    'n',
    function()
      local c = awful.client.restore()
      -- Focus restored client
      if c then
        _G.client.focus = c
        c:raise()
      end
    end,
    {description = 'restore minimized', group = 'client'}
  ),
  awful.key({ modkey }, "s", function ()
    awful.screen.focused().systray.visible = not awful.screen.focused().systray.visible
    end, {description = "Toggle systray visibility", group = "launcher"}
  ),
  -- Next variety wallpaper
  awful.key(
    {modkey},
    'n',
    function()
      awful.spawn(apps.default.variety)
    end,
    {description = 'next wallpaper', group = 'launcher'}
  ),
-- open calculator
  awful.key(
    {modkey},
    'c',
    function()
      awful.spawn(apps.default.calculator)
    end,
    {description = 'open calculator', group = 'launcher'}
  ),
  --[[open slack
  awful.key(
    {modkey},
    's',
    function()
      awful.spawn(apps.default.slack)
    end,
    {description = 'open slack', group = 'launcher'}
  ),]]
  -- Dropdown application
  awful.key(
    {modkey},
    'z',
    function()
      _G.toggle_quake()
    end,
    {description = 'dropdown application', group = 'launcher'}
  ),
    -- VirtualBox
    awful.key(
      {modkey, 'Shift'},
      'v',
      function()
        awful.util.spawn(apps.default.virtualBox)
      end,
      {description = 'VirtualBox', group = 'launcher'}
    ),
    -- Virt-Manager
    awful.key(
      {modkey},
      'v',
      function()
        awful.util.spawn(apps.default.virt)
      end,
      {description = 'Virt-Manager', group = 'launcher'}
    ),
  -- Widgets popups
  --[[awful.key(
    {altkey},
    'h',
    function()
      if beautiful.fs then
        beautiful.fs.show(7)
      end
    end,
    {description = 'Show filesystem', group = 'widgets'}
  ),]]
  awful.key(
    {altkey},
    'w',
    function()
      if beautiful.weather then
        beautiful.weather.show(7)
      end
    end,
    {description = 'Show weather', group = 'widgets'}
  ),
  -- Brightness
  awful.key(
    {},
    'XF86MonBrightnessUp',
    function()
      awful.spawn('xbacklight -inc 10')
    end,
    {description = '+10%', group = 'hotkeys'}
  ),
  awful.key(
    {},
    'XF86MonBrightnessDown',
    function()
      awful.spawn('xbacklight -dec 10')
    end,
    {description = '-10%', group = 'hotkeys'}
  ),
  -- ALSA volume control
  awful.key(
    {},
    'XF86AudioRaiseVolume',
    function()
      awful.spawn('amixer -D pulse sset Master 5%+', false)
      awesome.emit_signal('widget::volume')
			awesome.emit_signal('module::volume_osd:show', true)
    end,
    {description = 'volume up', group = 'hotkeys'}
  ),
  awful.key(
    {},
    'XF86AudioLowerVolume',
    function()
      awful.spawn('amixer -D pulse sset Master 5%-')
    end,
    {description = 'volume down', group = 'hotkeys'}
  ),
  --[[ mod + (Shift =)next song mod _ (shift -)previous song
  ]]
  awful.key(
    {modkey},
    '`',
    function()
      awful.util.spawn(apps.default.mocp)
    end,
    {description = 'Play/Pause Music', group = 'hotkeys'}
  ),
  awful.key(
    {modkey, 'Shift'},
    '`',
    function()
      awful.util.spawn(apps.default.moc)
    end,
    {description = 'Start music server', group = 'hotkeys'}
  ),
  awful.key(
    {modkey,},
    '.',
    function()
      awful.util.spawn(apps.default.mocf)
    end,
    {description = 'Forward Next Music', group = 'hotkeys'}
  ),
  awful.key(
    {modkey,},
    ',',
    function()
      awful.util.spawn(apps.default.mocb)
    end,
    {description = 'Backward Previous Music', group = 'hotkeys'}
  ),
  awful.key(
    {modkey},
    '=',
    function()
      awful.spawn('amixer -D pulse sset Master 5%+')
    end,
    {description = 'volume up', group = 'hotkeys'}
  ),
  awful.key(
    {modkey},
    '-',
    function()
      awful.spawn('amixer -D pulse sset Master 5%-')
    end,
    {description = 'volume down', group = 'hotkeys'}
  ),
  awful.key(
    {},
    'XF86AudioMute',
    function()
      awful.spawn('amixer -D pulse set Master 1+ toggle')
    end,
    {description = 'toggle mute 1', group = 'hotkeys'}
  ),
  awful.key(
    {},
    'XF86AudioNext',
    function()
      --
    end,
    {description = 'toggle mute 2', group = 'hotkeys'}
  ),
  awful.key(
    {},
    'XF86PowerDown',
    function()
      --
    end,
    {description = 'toggle mute 3', group = 'hotkeys'}
  ),
  awful.key(
    {},
    'XF86PowerOff',
    function()
      _G.exit_screen_show()
    end,
    {description = 'toggle mute 4', group = 'hotkeys'}
  ),
  -- Screen management
  awful.key(
    {modkey},
    'q',
    awful.client.movetoscreen,
    {description = 'move window to next screen', group = 'client'}
  ),
  -- Open default program for tag
  awful.key(
    {modkey},
    't',
    function()
      awful.spawn(
          awful.screen.focused().selected_tag.defaultApp,
          {
            tag = _G.mouse.screen.selected_tag,
            placement = awful.placement.bottom_right
          }
        )
    end,
    {description = 'Open default program for tag/workspace', group = 'tag'}
  ),
  -- Custom hotkeys
  -- vfio integration
  awful.key(
    {altkey, 'Control'},
    'space',
    function()
      awful.util.spawn_with_shell('vm-attach attach')
    end
  ),
  -- Gimp
  awful.key(
    {modkey},
    'g',
    function()
      awful.util.spawn(apps.default.brush)
    end,
    {description = 'Gimp', group = 'launcher'}
  ),
  
  --[[Lutris hotkey
  awful.key(
    {modkey},
    'g',
    function()
      awful.util.spawn_with_shell('lutris')
    end
  ),]]
  -- System Monitor hotkey
  awful.key(
    {altkey},
    'm',
    function()
      awful.util.spawn_with_shell('mate-system-monitor')
    end
  ),
  -- Kill VLC
  --[[awful.key(
    {modkey},
    'v',
    function()
      awful.util.spawn_with_shell('killall -9 vlc')
    end
  ),]]
  -- File Manager
  awful.key(
    {modkey},
    'e',
    function()
      awful.util.spawn(apps.default.files)
    end,
    {description = 'filebrowser', group = 'hotkeys'}
  ),
  -- File Manager on Terminal (ranger)
  awful.key(
    {modkey, 'Shift'},
    'e',
    function()
      awful.util.spawn(apps.default.ranger)
    end,
    {description = 'rangerbrowser', group = 'hotkeys'}
  ),
  -- Emoji Picker
  awful.key(
    {modkey},
    'a',
    function()
      awful.util.spawn_with_shell('ibus emoji')
    end,
    {description = 'Open ibus emoji picker', group = 'hotkeys'}
  )
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
  -- Hack to only show tags 1 and 9 in the shortcut window (mod+s)
  local descr_view, descr_toggle, descr_move, descr_toggle_focus
  if i == 1 or i == 9 then
    descr_view = {description = 'view tag #', group = 'tag'}
    descr_toggle = {description = 'toggle tag #', group = 'tag'}
    descr_move = {description = 'move focused client to tag #', group = 'tag'}
    descr_toggle_focus = {description = 'toggle focused client on tag #', group = 'tag'}
  end
  globalKeys =
    awful.util.table.join(
    globalKeys,
    -- View tag only.
    awful.key(
      {modkey},
      '#' .. i + 9,
      function()
        local screen = awful.screen.focused()
        local tag = screen.tags[i]
        if tag then
          tag:view_only()
        end
      end,
      descr_view
    ),
    -- Toggle tag display.
    awful.key(
      {modkey, 'Control'},
      '#' .. i + 9,
      function()
        local screen = awful.screen.focused()
        local tag = screen.tags[i]
        if tag then
          awful.tag.viewtoggle(tag)
        end
      end,
      descr_toggle
    ),
    -- Move client to tag.
    awful.key(
      {modkey, 'Shift'},
      '#' .. i + 9,
      function()
        if _G.client.focus then
          local tag = _G.client.focus.screen.tags[i]
          if tag then
            _G.client.focus:move_to_tag(tag)
            tag:view_only(tag)
          end
        end
      end,
      descr_move
    ),
    -- Toggle tag on focused client.
    awful.key(
      {modkey, 'Control', 'Shift'},
      '#' .. i + 9,
      function()
        if _G.client.focus then
          local tag = _G.client.focus.screen.tags[i]
          if tag then
            _G.client.focus:toggle_tag(tag)
          end
        end
      end,
      descr_toggle_focus
    )
  )
end

return globalKeys
