local awful = require('awful')
local gears = require('gears')
local client_keys = require('configuration.client.keys')
local client_buttons = require('configuration.client.buttons')
local scr = s or awful.screen.focused()
local tag = t or scr.selected_tag
local quake = require('configuration.apps').default.quake


-- Rules
awful.rules.rules = {
-- All clients will match this rule.
  {rule = {}, properties = {focus = awful.client.focus.filter, raise = true, keys = client_keys,
      buttons = client_buttons, screen = awful.screen.preferred, placement = awful.placement.no_offscreen,
      floating = false, maximized = false, above = false, below = false, ontop = false, sticky = false,
      maximized_horizontal = false, maximized_vertical = false}},
  {rule_any = {name = {quake}}, properties = {placement = awful.placement.centered,
      ontop = true, floating = true, drawBackdrop = true,
      shape = function() return function(cr, w, h)
          gears.shape.rounded_rect(cr, w, h, 8) end end, skip_decoration = true}},
      -- Floating Applications Exception
  {rule_any = {type = {'dialog'}, class = {'Wicd-client.py', 'Variety', 'Volumeicon', 'imagewriter', 'File-roller', 'Plank',
      'calendar.google.com', 'Pavucontrol', 'Xfce4-power-manager-settings', 'Xfce4-notes', 'Qalculate-gtk', 'Arandr', 'Kvantum Manager'}},
    properties = {placement = awful.placement.centered, ontop = true, floating = true, -- drawBackdrop = true,
      shape = function() return function(cr, w, h)
          gears.shape.rounded_rect(cr, w, h, 8) end end, skip_decoration = true}},
-- Floating Applications Exception
  {rule_any = {type = {'dialog'}, class = {'Plank',}},
  properties = {placement = awful.placement.centered, ontop = true, floating = true, screen = s, tag = t,
  shape = function() return function(cr, w, h)
      gears.shape.rounded_rect(cr, w, h, 8) end end, skip_decoration = true}},
-- Conky
  {rule = {instance = "Conky"}, properties = { floating = true, maximized = false, skip_decoration = true,
      shape = function() return function(cr, w, h) gears.shape.rounded_rect(cr, w, h, 8) end end,}},
-- Applications Assignment on Tags
  {--[[Brave]]rule = { instance = "brave-browser"}, properties = {--[[screen = "DP-1", ]]tag = "1"}},
  {--[[OBS]]rule = { instance = "obs"}, properties = {screen = "LVDS-1", tag = "2"}},
  {--[[Notion]]rule = { instance = "notion-app"}, properties = {screen = "LVDS-1", tag = "3"}},
  {--[[Social]]rule = { instance = "slack"}, properties = {screen = "LVDS-1", tag = "3"}},
  {--[[Social]]rule = { instance = "telegram-desktop"}, properties = {screen = "LVDS-1", tag = "3"}},
  {--[[Music]]rule = { instance = "vlc"}, properties = {tag = "4"}},
  {--[[Code]]rule = { instance = "code-oss"}, properties = {--[[screen = "LVDS-1", ]]tag = "5"}},
  {--[[Gimp]]rule = { instance = "gimp"}, properties = {tag = "5"}},
  {--[[Timeshift]]rule = { instance = "timeshift-launcher"}, properties = {screen = "LVDS-1", tag = "5"}},
  {--[[Gparted]]rule = { instance = "gpartedbin"}, properties = {screen = "LVDS-1", tag = "5"}},
  {--[[Virt-Manager]]rule = { instance = "virt-manager"}, properties = {screen = "LVDS-1", tag = "6"}},
  {--[[VirtualBox]]rule = { instance = "VirtualBox Manager"}, properties = {screen = "LVDS-1", tag = "6"}},
  {--[[Kdenlive]]rule = { instance = "kdenlive"}, properties = {--[[screen = "DP-1", ]]tag = "6"}},
  {--[[Audacium]]rule = { instance = "audacium"}, properties = {screen = "LVDS-1", tag = "6"}},
--[[
  {--[[Files]rule = { instance = "Thunar"}, properties = { tag = "2"}},
  {--[[gedit]rule = { instance = "gedit"}, properties = {screen = "LVDS-1", tag = "3"}},
  {--[[Terminal]rule = { instance = "Alacritty"}, properties = {screen = "LVDS-1", tag = "2"}},
-- Maximized Applications Exception
    {rule_any = {instance = {'code-oss', 'gimp', 'obs'}}, properties = { maximized = true, skip_decoration = true}},
-- Fullscreen
    {rule = {class = 'virt-manager'}, properties = {titlebars_enabled = false}},]]
}
