local awful = require('awful')
local gears = require('gears')
local mainmenu = require('module.mainmenu')
local modkey = require('configuration.keys.mod').modKey

root.buttons(gears.table.join(
  awful.button({}, 1, function() mainmenu.mainmenu:hide() end),
  awful.button({}, 3, function() mainmenu.mainmenu:show() end),
  awful.button({}, 4, awful.tag.viewprev),
  awful.button({}, 5, awful.tag.viewnext)
))

return awful.util.table.join(
  awful.button({      }, 1, function(c) _G.client.focus=c c:raise() end),
  awful.button({modkey}, 1, awful.mouse.client.move),
  --awful.button({      }, 2, awful.mouse.client.move),
  awful.button({modkey}, 3, awful.mouse.client.resize),
  awful.button({modkey}, 4, function() awful.layout.inc(1) end),
  awful.button({modkey}, 5, function() awful.layout.inc(-1) end)
)
